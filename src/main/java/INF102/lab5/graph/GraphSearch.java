package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    //Checks if there is a path between the two nodes inside the graph.
    public boolean connected(V u, V v) {
        Set<V> visited = new HashSet<>();
        return nodeIsConnected(u, v, visited);
    }

    //Method that checks for connection between nodes.
    private boolean nodeIsConnected(V u, V v, Set<V> visited) {
        if (u.equals(v)) {
            return true;
        }
        visited.add(u);

        for (V node : graph.getNeighbourhood(u)) {
            if (!visited.contains(node) && graph.hasEdge(u, node)) {
                if (nodeIsConnected(node, v, visited)) {
                    return true;
                }
            }
        }
        return false;

    }

}
